function PalindromeTwo(str) { 

    // code goes here  
    
    let minusculas = str.toLowerCase();
    let noChract = minusculas.replace(/[^a-zA-Z 0-9.]+/g,' ')
    
    let phrase = noChract.split(' ').join('');
    let inverse = noChract.split(' ').join('').split('').reverse().join('');
   
  if (phrase === inverse) {
    str = true;
  } else {
    str = false;
  }
         return str;
  }
     
  // keep this function call here 
  PalindromeTwo(readline());