function SimpleSymbols(str) { 

  let string = str.split('');
  let abc = 'abcdefghijklmnopqrstuvwxyz'.split('');
  
  for (let i=0; i < string.length; i++) {
    if (abc.indexOf(string[i]) !== -1 && string[i]) {
      return false;
    } else if ( abc.indexOf(string[i]) !== -1 && string[string.length]) {
      return false;
    } else if (string[i] === '+' && string[i+2] === '+') {
      return true;
    }
  }
         
}
   
SimpleSymbols("++d+===+c++==a");