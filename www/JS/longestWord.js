function LongestWord(sen) { 

  sen = sen.replace(/[\W]+/g, ' ');
  let nuevoArray = sen.split(' ');
  const word = nuevoArray.reduce((accumulator, currentValue) => {
    if ( accumulator.length < currentValue.length) {
      return currentValue;
    } else {
      return accumulator;
    }
  });
  
  return word;

         
}
   
LongestWord("Hola, me llamo Ana");